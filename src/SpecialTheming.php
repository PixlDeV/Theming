<?php
namespace MediaWiki\Extension\Theming;

use ContentHandler;
use Html;
use HTMLForm;
use MediaWiki\CommentStore\CommentStoreComment;
use MediaWiki\Page\WikiPageFactory;
use MediaWiki\Revision\RevisionRecord;
use MediaWiki\Revision\SlotRecord;
use MediaWiki\Title\Title;
use OOUI;
use MediaWiki\Context\RequestContext;
class SpecialTheming extends \SpecialPage {
    function __construct(WikiPageFactory $wikiPageFactory) {
        parent::__construct( 'Theming' );
		$this->factory = $wikiPageFactory;
    }
    function execute($par) {
        // $this->setHeaders();
        $request = $this->getRequest();
        $output = $this->getOutput();
		$config = $output->getConfig();

		$title = Title::newFromText($config->get("ThemingStorePage"));
		$page = $this->factory->newFromTitle( $title );
		$content = $page->getContent( RevisionRecord::RAW );
		$text = $content->getText();
//		return $text;
		$defaults = json_decode( $text, true);
//		$output->addHTML( Html::element( 'p', [], $defaults) );
//		return;
        // $output->enableOOUI();
        // formDescriptor Array to tell HTMLForm what to build
        $formDescriptor = [
            'baselinkcolor' => [
                'label' => 'Base Link Color', // Label of the field
                'class' => 'HTMLTextField', // Input type
                'cssclass' => 'colorpicker',
                'help' => 'The base color for links',
				'default' => $defaults['baselinkcolor']
            ]
        ];

        // Build the HTMLForm object
        $htmlForm = HTMLForm::factory('table', $formDescriptor, $this->getContext(), 'colorform' );

        // Text to display in submit button
        $htmlForm->setSubmitText( 'Update' );
        $htmlForm->setSubmitCallback( [$this, 'processCallback'] );
        $htmlForm->show(); // Display the form

        $output->addModules( [ 'ext.Theming' ] );
        $output->setPageTitle( "Theming" );

    }

    function processCallback( $data ) {
		$output = $this->getOutput();
		$config = $output->getConfig();
		$actor = $this->getUser();
		$title = Title::newFromText($config->get("ThemingStorePage"));
		$page = $this->factory->newFromTitle( $title );
		$updater = $page->newPageUpdater( $actor );
		$content = ContentHandler::makeContent( json_encode($data), $title );
		$updater->setContent( SlotRecord::MAIN, $content);
		$updater->saveRevision(CommentStoreComment::newUnsavedComment('Updating Theming values per ' . $actor->getName()), );
		$status = $updater->getStatus();
		$notExploded = $status->isOK();
		if (!$notExploded) {
			return "An error occured. No I don't know what bye.";
		}
        else {
			return "New Base Link Color:" . $data['baselinkcolor'];
		}
    }

    function getGroupName() {
        return 'other';
    }

}
